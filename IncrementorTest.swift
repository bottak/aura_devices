import XCTest
@testable import Incrementor

class IncrementorTest: XCTestCase {
    
    let minValue: UInt = UInt.min
    
    // Check: create an instance with default values and get current value
    func testIncrementor_createAndGet() {
        let incrementor = Incrementor()
        
        XCTAssertEqual(incrementor.getNumber(),
                       minValue,
                       "The value of just created instance by default should be '0'")
    }
    
    // Check: increment and compare value
    func testIncrementor_increment() {
        let incrementor = Incrementor()
        
        let iterationsCount = UInt.random(in: minValue + 10..<1000)
        
        for _ in 1...iterationsCount {
            incrementor.incrementNumber()
        }
        
        XCTAssertEqual(incrementor.getNumber(),
                       iterationsCount,
                       "Increment operation should increase value by '1'")
    }
    
    // Check: reach upper bound and check to loop
    func testIncrementor_incrementOverMaxBound() {
        let maxBound = UInt.random(in: minValue + 10..<1000)
        
        let incrementor = Incrementor(maxBound)
        
        for _ in 1...maxBound {
            incrementor.incrementNumber()
        }
        
        XCTAssertEqual(incrementor.getNumber(),
                       minValue,
                       "When value reaches upper bound, it should be reset to '0'")
    }
    
    // Check: init with max value and increment over the bound and check to loop
    func testIncrementor_initWithMaxValueParameter() {
        let max = UInt.random(in: minValue..<1000)
        
        let incrementor = Incrementor(max)
        
        while incrementor.getNumber() < max - 1 {
            incrementor.incrementNumber()
        }
        
        incrementor.incrementNumber()
        
        XCTAssertEqual(incrementor.getNumber(),
                       minValue,
                       "Should correct initialize instance with 'maxValue' parameter")
    }
    
    // Check: set max value then try to reach it and check to loop
    func testIncrementor_setMaximumValue() {
        let max = UInt.random(in: minValue..<1000)
        
        let incrementor = Incrementor()
        incrementor.setMaximumValue(max)
        
        while incrementor.getNumber() < max - 1 {
            incrementor.incrementNumber()
        }
        
        incrementor.incrementNumber()
        
        XCTAssertEqual(incrementor.getNumber(),
                       minValue,
                       "Should set upper bound")
    }
    
    // Check: increment then set max value less than current iterator's value
    func testIncrementor_setMaxLessThanIterator() {
        let incrementor = Incrementor()
        
        let incrementCount = UInt.random(in: minValue..<100)
        
        while incrementor.getNumber() < incrementCount {
            incrementor.incrementNumber()
        }
        
        let newMax = UInt.random(in: minValue..<incrementCount)
        
        incrementor.setMaximumValue(newMax)
        
        XCTAssertEqual(incrementor.getNumber(),
                       minValue,
                       "Set max value less than current value should reset iterator value to '0'")
    }
    
    // Check: set max value equal to lower bound and try to increment then check value
    func testIncrementor_setMaxValueEqualsLowerBound() {
        let incrementor = Incrementor(0)
        incrementor.incrementNumber()
        XCTAssertEqual(incrementor.getNumber(),
                       minValue,
                       "Set max value equal to lower bound and iterator value should be")
    }
    
    // Check: increment value n times in one thread and do the same thing in another thread
    func testIncrementor_checkRaceConditionWhenIncrementing() {
        let incrementor = Incrementor(3000)
        
        let iterationsCount: UInt = 1000
        
        let group = DispatchGroup()

        group.enter()
        let thread1 = Thread {
            for _ in 0..<iterationsCount {
                incrementor.incrementNumber()
            }

            group.leave()
        }

        thread1.start()

        group.enter()
        let thread2 = Thread {
            for _ in 0..<iterationsCount {
                incrementor.incrementNumber()
            }

            group.leave()
        }

        thread2.start()
        
        group.wait()
        
        XCTAssertEqual(incrementor.getNumber(),
                       iterationsCount * 2,
                       "Not thread safe")
    }
}
