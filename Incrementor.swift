import Foundation

public final class Incrementor {
    private var iterator: UInt = UInt.min
    private var maxBound: UInt
    
    /// Mutex to avoiding race condition when incrementing iterator
    private let incrementorLock = NSLock()
    
    init(_ maxValue:UInt = UInt.max) {
        maxBound = maxValue
    }
    
    private func resetValue() {
        iterator = UInt.min
    }
}

// MARK: - Required Interface
extension Incrementor {
    
    /// Provides iterator current value.
    func getNumber() -> UInt {
        return iterator
    }
    
    /// Increments iterators value by `1`
    func incrementNumber() {
        guard maxBound > UInt.min else { return }
        
        // Mutex lock
        incrementorLock.lock()
        
        iterator += 1
        
        if iterator == maxBound { resetValue() }
        
        // Mutex unlock
        incrementorLock.unlock()
    }
    

    func setMaximumValue(_ maxValue: UInt) {
        defer { maxBound = maxValue }

        guard iterator < maxValue else {
            resetValue()
            return
        }
    }
}

// MARK: - CustomDebugStringConvertible
extension Incrementor: CustomDebugStringConvertible {
    public var debugDescription: String {
        return "Current value is \(iterator) in the range [\(UInt.min); \(maxBound))"
    }
}
